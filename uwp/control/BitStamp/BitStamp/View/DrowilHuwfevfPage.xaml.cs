﻿using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using BitStamp.ViewModel;
using lindexi.uwp.Framework.ViewModel;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

namespace BitStamp
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class DrowilHuwfevfPage : Page
    {
        public DrowilHuwfevfPage()
        {
            this.InitializeComponent();

            ViewModel = (DrowilHuwfevfModel) DataContext;

            ViewModel.Content = KasibkqeStkxaij;

            ViewModel.ViewModel = new List<ViewModelPage>()
            {
                new ViewModelPage(typeof(HrbHtladModel), typeof(HrbHtlad)),
            };

            TadSvc();
        }

        private async void TadSvc()
        {
            if (!_senKrobe)
            {
                _senKrobe = true;
                ViewModel.Navigate(typeof(HrbHtladModel), AccoutGoverment.AccountModel.Account);
            }
        }

        private bool _senKrobe;

        public DrowilHuwfevfModel ViewModel { get; set; }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            SystemNavigationManager telTtxxskne = SystemNavigationManager.GetForCurrentView();
            telTtxxskne.BackRequested += BackRequested;

            telTtxxskne.AppViewBackButtonVisibility =
                AppViewBackButtonVisibility.Visible;

            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;

            var dmbyzkfscDycoue = ApplicationView.GetForCurrentView();

            dmbyzkfscDycoue.TitleBar.BackgroundColor = Colors.Black;

            dmbyzkfscDycoue.TitleBar.ButtonBackgroundColor = Colors.Transparent;

            ApplicationView.PreferredLaunchViewSize = new Size(1024, 800);

            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;

            dmbyzkfscDycoue.SetPreferredMinSize(new Size(1024, 800));
        }

        private void BackRequested(object sender, BackRequestedEventArgs e)
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }
    }
}